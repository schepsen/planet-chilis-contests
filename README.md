## PROJECT ##

* ID: **P**lanet**C**hili!**C**ONTESTS
* Contact: schepsen@web.de

## DESCRIPTION ##

The solutions to the Planet Chili's Coding Challenges

## USAGE ##

    mkdir bin  
    cd bin
    cmake ..  
    make
    ./challenge${n}


## CHANGELOG ##

### PlanetChili!CONTESTS 0.1, updated @ 2017-09-21 ###

* Solved Challenge 01
* Solved Challenge 02

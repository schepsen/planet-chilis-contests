#include <fstream>
#include <bitset>
#include <chrono>
#include <string>
#include <ctime>
#include <iostream>

// #define TEST_MY_OWN_RANDOM_NUMBERS

inline void sub(std::string& number)
{
    std::string::iterator end = number.end() - 1;

    while(number.begin() < end)
    {
        if((*end) > '0')
        {
            *end -= 1; return;
        }
        else
        {
            *(end--) = '9';
        }
    }
    if(number.front() == '1' && number.size() > 1)
    {
        *end = '9'; number.resize(number.size() - 1);
    }
    else
    {
        *end -= 1;
    }
}

inline void add(std::string& number)
{
    std::string::iterator end = number.end() - 1;

    while(number.begin() <= end)
    {
        if((*end) < '9')
        {
            *end += 1; return;
        }
        else
        {
            *(end--) = '0';
        }
    }

    number.front() = '1'; number.resize(number.size() + 1, '0');
}

inline void split(std::string& number)
{
    std::string tmp = ""; int i = number.front() - '0';
    std::string::const_iterator it = number.cbegin();

    while(i < 2)
    {
        i = i * 10 + *(++it) - '0';
    }

    for(std::string::const_iterator cit = it; cit < number.cend();)
    {
        tmp += (i >> 1) + '0';
        i = (i & 1) * 10 + (*(++cit)) - '0';
    }

    number = tmp; // There is no throwable errors, DONT divide numbers less then 2
}

inline bool lsb(std::string& number)
{
    return ((((number.back() - '0') + (10 * (*(number.end() - 2) - '0'))) & 3) == 1);
}

int main(int argc, char** argv)
{
    std::string filename = "number.txt";
    std::string number = ""; // Define it if u want to test some values manually

#ifdef TEST_MY_OWN_RANDOM_NUMBERS

    srand((int) time(0));

    for(int i = 0; i < 10000; ++i)
    {
        number.append(std::to_string(rand() % 10));
    }

#else

    if(argc > 1)
    {
        filename = argv[1];
    }

    std::ifstream ifs(filename, std::ifstream::in);

    if(ifs.is_open())
    {
        while(std::getline(ifs, number));
    }
    else
    {
       std::cerr << "Attention! The file input is not found!" << std::endl; return 0;
    }

#endif

    auto start = std::chrono::steady_clock::now();

    long long answer = 0;

    while(number != "1")
    {
        if(!((number.back() - '0') & 1)) // The number is even
        {
            split(number);
        }
        else if(lsb(number) || number == "3") // The LSBs are 01
        {
            sub(number);
        }
        else { add(number); } answer++;
    }

    auto end = std::chrono::steady_clock::now();

    std::cout << "The minimal number of stepts: " << answer  << "\nElapsed time in milliseconds: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;

    return 0;
}

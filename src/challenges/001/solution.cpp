#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <iostream>

int main(int argc, char** argv)
{
	std::string filename = "pyramid.txt";

	if(argc > 1)
	{
		filename = argv[1];
	}

	std::ifstream ifs(filename, std::ifstream::in);

	std::vector<std::vector<int> > v;
	int x;
	std::string line;

	while (std::getline(ifs, line))
	{
		std::stringstream stream(line);

		std::vector<int> row;

		while(stream >> x) { row.push_back(x); } v.push_back(row);
	}

	ifs.close();

	for (unsigned i = 0; i < v.size() - 1; ++i)
	{
		for (unsigned j = 0; j < v[i].size(); ++j)
		{
			if(j == 0)
			{
				v[i + 1][j] += v[i][j];

				if(i > 0 && j != v[i].size() - 1)
				{
					v[i + 1][j + 1] += std::max(v[i][j], v[i][j + 1]);
				}
			}
			if(j == v[i].size() - 1)
			{
				v[i + 1][j + 1] += v[i][j];
			}
			if(i > 0 && j != v[i].size() - 1 && j != 0)
			{
				v[i + 1][j + 1] += std::max(v[i][j], v[i][j + 1]);
			}
		}
	}

	std::sort(v.back().begin(), v.back().end()); std::cout << v.back().back() << std::endl; return 0;
}
